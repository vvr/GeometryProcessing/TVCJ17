if !isdefined(:GeometryProcessing)
    include("../../GeometryProcessing/src/GeometryProcessing.jl")
end

using Base.Iterators
using GeometryProcessing

function bsvd(anm::Animation; frames_per_block = 40, components = 50,
              data_qbits = 12, dict_qbits = 12)
    # iterator for blocks of frames across the animation
    blocks = Segmentation(anm.data, :frames, frames_per_block)
    # arrangement of blocks in 2D matrices
    arrangement = [[:coors, :frames], :vertices]
    reconstructed = anm.data[(:frames, 1:0)]
    for block in blocks
        block, restore = arrange_with_inverse(block, arrangement)
        dict = svd(block * block')[1][:, 1:components]
        block = restore(transmit(block, dict, data_qbits, dict_qbits))
        reconstructed = cat(:frames, reconstructed, block)
    end
    # Copy animation and replace the reconstructed data
    reconstructed_anm = deepcopy(anm)
    reconstructed_anm.data = reconstructed
    reconstructed_anm
end

function baoi(anm::Animation; frames_per_block = 40, components = 50,
              data_qbits = 12, dict_qbits = 12)
    # iterator for blocks of frames across the animation
    blocks = Segmentation(anm.data, :frames, frames_per_block)
    # arrangement of blocks in 2D matrices
    arrangement = [[:coors, :frames], :vertices]
    # initial block and its dictionary as computed by SVT
    block, restore = arrange_with_inverse(first(blocks), arrangement)
    dict = svd(block * block')[1][:, 1:components]
    reconstructed = restore(transmit(block, dict, data_qbits, dict_qbits))
    # transmit the rest of the blocks through bandwidth-consistent AOI
    for block in drop(blocks, 1)
        block, restore = arrange_with_inverse(block, arrangement)
        dict = qr(block * block' * dict[1:size(block, 1), :])[1]
        block = restore(transmit(block, dict, data_qbits, dict_qbits))
        reconstructed = cat(:frames, reconstructed, block)
    end
    # Copy animation and replace the reconstructed data
    reconstructed_anm = deepcopy(anm)
    reconstructed_anm.data = reconstructed
    reconstructed_anm
end

function qaoi(anm::Animation; frames_per_block = 40, components = 50,
              data_qbits = 12, dict_qbits = 12, error_metric = kg_error,
              error_bounds::Tuple{Float64, Float64} = (-50, -40))
    expand_dict(dict::Matrix) = [dict normalize(randn(size(dict, 1)))]
    trim_dict(dict::Matrix) = dict[:, 1:end-1]
    # iterator for blocks of frames across the animation
    blocks = Segmentation(anm.data, :frames, frames_per_block)
    # arrangement of blocks in 2D matrices and establishement of error bounds
    arrangement = [[:coors, :frames], :vertices]
    low_bound, high_bound = error_bounds
    # initial block and its dictionary as computed by SVT
    block, restore = arrange_with_inverse(first(blocks), arrangement)
    dict = svd(block * block')[1][:, 1:components]
    reconstructed = restore(transmit(block, dict, data_qbits, dict_qbits))
    # transmit the rest of the blocks through quality-consistent AOI
    for block in drop(blocks, 1)
        # computation of autocorrelation and dictionary
        flat_block, restore = arrange_with_inverse(block, arrangement)
        block_autocor = flat_block * flat_block'
        dict = qr(block_autocor * dict[1:size(flat_block, 1), :])[1]
        # reconstructed block and error computation
        recon_block = restore(dict * dict' * flat_block)
        error = error_metric(block, recon_block)
        # adapt the dictionary to the specified error bounds
        while !(low_bound < error < high_bound)
            if error > high_bound
                dict = qr(block_autocor * expand_dict(dict))[1]
                println("Increasing dictionary size $(size(dict)).")
            elseif error < low_bound
                dict = qr(block_autocor * trim_dict(dict))[1]
                println("Decreasing dictionary size $(size(dict)).")
            else
                # println("Block reconstruction error: $error.")
                break
            end
            recon_block = restore(dict * dict' * flat_block)
            error = error_metric(block, recon_block)
            println("Reconstruction error: $error.")
        end
        println("Final reconstruction error: $error.\n")
        block = restore(transmit(flat_block, dict, data_qbits, dict_qbits))
        reconstructed = cat(:frames, reconstructed, block)
    end
    # Copy animation and replace the reconstructed data
    reconstructed_anm = deepcopy(anm)
    reconstructed_anm.data = reconstructed
    reconstructed_anm
end
