<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#sec-1">1. Adaptive Compression of Animated Meshes by Exploiting Orthogonal Iterations</a></li>
<li><a href="#sec-2">2. Execution</a></li>
</ul>
</div>
</div>

# Adaptive Compression of Animated Meshes by Exploiting Orthogonal Iterations<a id="sec-1" name="sec-1"></a>

The included code contains the implementation of algorithms for adaptive
compression of animated meshes by exploiting orthogonal iterations, as
presented in the publication "Aris S. Lalos, A. A. Vasilakis, A. Dimas,
K. Moustakas, "Adaptive Compression of Animated Meshes by Exploiting
Orthogonal Iterations", The Visual Computer, 33(6-8): 811-821 (2017)".

# Execution<a id="sec-2" name="sec-2"></a>

To execute the supplied code, one should follow the steps:

1.  Obtain the files locally either by downloading the compressed archive and
    decompressing it, or by cloning the git repository.

2.  Obtain the GeometryProcessing library from and place it in the same
    directory alongside the "Showcase" directory that was created in the
    previous step.

3.  Make sure that the each animation's files are all placed in the same
    directory, have at least one common part in their names, are
    sequentially numbered and in Wavefront obj format.

4.  Start Julia and install the WriteVTK package by executing the command
    \`Pkg.add("WriteVTK")\`.

5.  Afterwards, load the demo.jl file, by executing the command
    \`include("*path/to/demo.jl")\`, replacing the the "/path/to*" part with the
    actual path to the demo.jl file.

6.  After the loading is completed, you can see the value of the variable \`sp\`,
    which is a dictionary containing several key-value pairs that control the
    behaviour of the algorithms:
    -   \`:in<sub>dir\`</sub>: the input directory for the animated mesh
    -   \`:in<sub>filename\`</sub>: a common part of the input filenames
    -   \`:in<sub>filetype\`</sub>: the input files extension (only "obj" is supported)
    -   \`:out<sub>dir\`</sub>: the output directory for the reconstructed meshes
    -   \`:out<sub>filename\`</sub>: the name of the output files
    
    TBC