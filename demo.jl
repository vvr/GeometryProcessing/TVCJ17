include("tvcj17.jl")

sp = Dict(:in_dir => expanduser("~/data/vvr-dataset/handstand/"),
          :in_filename => "mesh",
          :in_filetype => "obj",
          :method => baoi,
          :frames_per_block => 25,
          :components => 40,
          :data_qbits => 12,
          :dict_qbits => 12,
          :error_bounds => (-40.0, -30.0),
          :out_dir => expanduser("~/data/exported/tvcj/"),
          :out_filename => "hs_baoi")

function demo(method)
    anm = Animation(sp[:in_dir], sp[:in_filename], sp[:in_filetype])
    if method == baoi
        anm_compr = baoi(anm,
                         frames_per_block = sp[:frames_per_block],
                         components = sp[:components],
                         data_qbits = sp[:data_qbits],
                         dict_qbits = sp[:dict_qbits])
    elseif method == qaoi
        anm_compr = qaoi(anm,
                         frames_per_block = sp[:frames_per_block],
                         components = sp[:components],
                         data_qbits = sp[:data_qbits],
                         dict_qbits = sp[:dict_qbits],
                         error_bounds = sp[:error_bounds])
    else error("Unknown compression method.")
    end
    add_vertex_data!(anm_compr,
                     :vertex_distances,
                     vertex_error(anm.data, anm_compr.data))
    export_animation_pvd(anm_compr, sp[:out_dir], sp[:out_filename])
end
